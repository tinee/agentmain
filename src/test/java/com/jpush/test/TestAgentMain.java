package com.jpush.test;

import com.sun.tools.attach.*;

import java.io.IOException;
import java.util.List;

public class TestAgentMain {
    public static void main(String[] args) throws IOException, AttachNotSupportedException, AgentLoadException, AgentInitializationException {
//        通过VirtualMachine类的attach(pid)方法，便可以attach到一个运行中的java进程上，
//        之后便可以通过loadAgent(agentJarPath)来将agent的jar包注入到对应的进程，
//        然后对应的进程会调用agentmain方法。

//        既然是两个进程之间通信那肯定的建立起连接，VirtualMachine.attach动作类似TCP创建连接的三次握手，
//        目的就是搭建attach通信的连接。而后面执行的操作，例如vm.loadAgent，
//        其实就是向这个socket写入数据流，接收方target
//        VM会针对不同的传入数据来做不同的处理。
        List<VirtualMachineDescriptor> list = VirtualMachine.list();

        for (VirtualMachineDescriptor vmd : list) {
            System.out.println(vmd.displayName());
            if ("com.jpush.test.Client".equals(vmd.displayName())){

                VirtualMachine virtualMachine = VirtualMachine.attach(vmd.id());
                virtualMachine.loadAgent("/Users/liyh/dev/GitSpace/agentmain/target/agentmain-1.0-SNAPSHOT.jar");
                virtualMachine.detach();
            }
        }

    }
}
