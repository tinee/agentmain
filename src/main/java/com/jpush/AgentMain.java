package com.jpush;

import java.lang.instrument.*;

public class AgentMain {
    public static void agentmain(String agentArgs, Instrumentation inst) throws UnmodifiableClassException, ClassNotFoundException {
        System.out.println("begin agent...");
        ClassDefinition def = new ClassDefinition(Person.class,Transformer.getBytesFromFile(Transformer.classNumberReturns2));
        inst.redefineClasses(new ClassDefinition[] { def });
        System.out.println("replace success");
    }

}
