* 启动时加载instrument agent过程：
```yaml
创建并初始化 JPLISAgent；

监听 VMInit 事件，在 JVM 初始化完成之后做下面的事情：

创建 InstrumentationImpl 对象 ；

监听 ClassFileLoadHook 事件 ；

调用 InstrumentationImpl 的loadClassAndCallPremain方法，在这个方法里会去调用 javaagent 中 MANIFEST.MF 里指定的Premain-Class 类的 premain 方法 ；

解析 javaagent 中 MANIFEST.MF 文件的参数，并根据这些参数来设置 JPLISAgent 里的一些内容。

```



* 运行时加载instrument agent过程：
```yaml
通过 JVM 的attach机制来请求目标 JVM 加载对应的agent，过程大致如下：

创建并初始化JPLISAgent；
解析 javaagent 里 MANIFEST.MF 里的参数；
创建 InstrumentationImpl 对象；
监听 ClassFileLoadHook 事件；
调用 InstrumentationImpl 的loadClassAndCallAgentmain方法，在这个方法里会去调用javaagent里 MANIFEST.MF 里指定的Agent-Class类的agentmain方法。


```

